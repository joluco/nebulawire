#!/bin/bash

docker run -it -v $(pwd)/../src/clearlinux_org/themes/clearlinux_theme:/work acidaniel/frontend:susy bower install --allow-root
docker run -it -v $(pwd)/../src/clearlinux_org/themes/clearlinux_theme:/work acidaniel/frontend:susy gulp clean:styleguide
docker run -it -v $(pwd)/../src/clearlinux_org/themes/clearlinux_theme:/work acidaniel/frontend:susy gulp build
