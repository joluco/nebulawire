#!/bin/bash

#To sync files down from the Servcer to a dev environment, run this from the project root:

echo "Syncing files..."

cd ..
rsync -rltvPh stg-sites2.vlan24.clearlinux.org:/srv/www/clearlinux.org/docroot/sites/default/files docroot/sites/default/
