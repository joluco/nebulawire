#!/bin/bash -x
ROOTPATH="${PWD}/../"
PROJECTDIR=$ROOTPATH"clear-linux-documentation"
JSONDIR=json_files
JSONPATH=$ROOTPATH$JSONDIR

if [ ! -d $JSONPATH ]; then
	cd $ROOTPATH
	mkdir -p json_files
fi

cd $JSONPATH
rm -rf *
$(PWD)
ls -l

cd $PROJECTDIR

ls -l

echo "Unpublishing Documentation Nodes..."

curl -k --request GET \
-u DocuBot:$(cat $ROOTPATH/.docuRest|base64 -D) \
--header 'Content-Type: application/json' \
http://clearlinux-local:8030/api/postdocumentation\?_format=json

RESULT=$(sphinx-build -b json source/. ../json_files | grep 'writing output...')

find $JSONPATH -name '*.fjson' -type f  -exec curl -k --request POST \
-u DocuBot:$(cat $ROOTPATH/.docuRest|base64 -D) \
--header 'Content-Type: application/json' \
http://clearlinux-local:8030/api/postdocumentation\?_format=json \
--data @{} \; -exec printf '\n' \;

echo "Sync Images..."

cd $JSONPATH

rsync -avz $JSONPATH"/_images/" $ROOTPATH"/docroot/sites/default/files/_images/"
