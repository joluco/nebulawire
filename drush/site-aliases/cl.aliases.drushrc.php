<?php

// Site clearlinux environment stage
$aliases['stg'] = array(
  'root' => '/srv/www/s1/clearlinux.org/docroot',
  'uri' => 's1.stgweb2.ostc.intel.com',
  'remote-host' => 'stgweb2.ostc.intel.com',
  'path-aliases' => array(
    '%drush' => '/srv/www/s1/clearlinux.org/vendor/bin/drush',
    '%drush-script' => '/srv/www/s1/clearlinux.org/vendor/drush/drush/drush.php',
    '%files' => '/srv/www/s1/clearlinux.org/docroor/sites/defatult/files/'
  )
);

$aliases['local'] = array(
  'uri' => 'clearlinux-local',
  'root' => '~/clearlinux.org/docroot'
);
