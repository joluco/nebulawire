<?php
/**
 * @file DrupalParagraphContext.php
 * A behat context for testing Drupal paragraphs.
 *
 * @copyright 2017 Palantir.net
 */

use Behat\Gherkin\Node\TableNode;
use Palantirnet\PalantirBehatExtension\Context\SharedDrupalContext;

class DrupalParagraphContext extends SharedDrupalContext
{

  /**
   * @Given the :type paragraph type has the expected fields:
   *
   * @param $type         string   The paragraph type to check.
   * @param $fieldsTable TableNode The list of fields to look for.
   *
   * @throws Exception
   */
  public function assertParagraphFields($type, TableNode $fieldsTable)
  {

    $this->visitPath("admin/structure/paragraphs_type/{$type}/form-display");

    /** @var Drupal\Core\Entity\Display\EntityFormDisplayInterface $form */
    $form = Drupal::entityTypeManager()
      ->getStorage('entity_form_display')
      ->load("paragraph.{$type}.default");

    if (null === $form) {
      throw new Exception("The {$type} paragraph did not have any fields.");
    }

    $fields = $form->getComponents();


    foreach ($fieldsTable->getHash() as $row) {
      $expected_field = $row['field'];

      if (!array_key_exists($expected_field, $fields)) {
        throw new Exception("Could not find field \"{$expected_field}\" on the {$type} paragraph.");
      }
      elseif ($fields[$expected_field]['type'] != $row['type']) {
        throw new Exception("Expected the field, \"{$expected_field}\" on the {$type} paragraph to be {$row['type']}, but found {$fields[$expected_field]['type']}.");
      }

    }
  }
}