@api
Feature: CLW2-XXX-Story-of-Paragraphs
  As a content creator
  I want to use paragraphs
  So I can add components to my site.

  Scenario: Image and Text Paragraphs should exist.
    Given I am logged in as a user with the administrator role
    When I visit "admin/structure/paragraphs_type"
    Then I should see "banner"
    And should see "call_to_action"
    And should see "community_card"
    And should see "dynamic_content_block"
    And should see "hero_image"
    And should see "hero_video"
    And should see "image_block"
    And should see "text_block"
    And should see "project_browser"
    And should see "sidebar_"
    And should see "text_block_full_width"
    And should see "two_column"
    And should see "video_block"
