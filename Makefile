include .env

db_directory=./mariadb-init

all: | include build install info dump

include:
ifeq ($(strip $(COMPOSE_PROJECT_NAME)),projectname)
#todo: ask user to make a project name and mv folders.
$(error Project name can not be default, please edit ".env" and set COMPOSE_PROJECT_NAME variable.)
endif

dump:
ifneq ("$(wildcard $(db_directory))", "")
	@echo "$(db_directory) Directory exists"
else
	@echo "$(db_directory) Directory does not exist, creating it."
	mkdir -p $(db_directory)
endif
	cd $(db_directory) && wget ${CI_DUMP}_`date +'%Y-%m-%d'`.sql.gz
	@echo "Database dumped into $(db_directory)..."

build: clean
	mkdir -p docroot
	mkdir -p db/${COMPOSE_PROJECT_NAME}

install:
	@echo "Updating containers..."
	docker-compose pull
	@echo "Build and run containers..."
	docker-compose up -d
	docker-compose exec -T php composer global require -o --update-no-dev --no-suggest hirak/prestissimo
	docker-compose exec php composer install; \
	basmake -s si

si:
	echo "Installing site $(PROFILE_NAME)"
	docker-compose exec -T php drush --root=/var/www/html/docroot si lightning --db-url=mysql://$(PROFILE_NAME):$(PROFILE_NAME)@mariadb/$(PROFILE_NAME) --account-pass=admin --site-name="$(PROFILE_NAME)" -y
#	docker-compose exec -T php drush --root=/var/www/html/docroot site-install config_installer config_installer_sync_configure_form.sync_directory='../conf' --yes
	docker-compose exec -T php drush --root=/var/www/html/docroot upwd admin --password=admin


chown:
	docker-compose exec -T php /bin/sh -c "chmod -R 777 /var/www/html/db/${COMPOSE_PROJECT_NAME}"
	docker-compose exec -T php /bin/sh -c "chown $(shell id -u):$(shell id -g) /var/www/html -R"
	docker-compose exec -T php /bin/sh -c "chown www-data: /var/www/html/docroot/sites/default/files -R"

info:
ifeq ($(shell docker inspect --format="{{ .State.Running }}" $(COMPOSE_PROJECT_NAME)_web 2> /dev/null),true)
	@echo Project IP: "localhost:"$(shell docker inspect --format='{{(index (index .NetworkSettings.Ports "80/tcp") 0).HostPort}}' $(COMPOSE_PROJECT_NAME)_web)
endif

exec:
	docker-compose exec php ash \

front:
	@echo "Building front tasks..."
	docker pull acidaniel/frontend:susy
	docker run --rm -v $(shell pwd)/src/$(PROFILE_NAME)/themes/$(THEME_NAME):/work acidaniel/frontend:susy bower install --allow-root
	docker run --rm -v $(shell pwd)/src/$(PROFILE_NAME)/themes/$(THEME_NAME):/work acidaniel/frontend:susy

phpcs:
	docker run --rm \
                -v $(shell pwd)/src/$(PROFILE_NAME):/work/profile \
                acidaniel/php7:sniffers phpcs -s --colors \
                --standard=Drupal,DrupalPractice \
                --extensions=php,module,inc,install,profile,theme,yml \
                --ignore=*.css,*.md,*.js,*.info.yml .
	docker run --rm \
                -v $(shell pwd)/src/$(PROFILE_NAME):/work/profile \
                acidaniel/php7:sniffers phpcs -s --colors \
                --standard=Drupal,DrupalPractice \
                --extensions=js \
                --ignore=*.css,sticky,waypoint.js,*.min.js,*.md,libraries/*,*.info.yml,styleguide/* .
phpcbf:
	docker run --rm \
                -v $(shell pwd)/src/$(PROFILE_NAME):/work/profile \
                acidaniel/php7:sniffers phpcbf -s --colors \
                --standard=Drupal,DrupalPractice \
                --extensions=php,module,inc,install,profile,theme,yml,txt,md \
                --ignore=*.css,*.md,*.js,*.info.yml .
	docker run --rm \
                -v $(shell pwd)/src/$(PROFILE_NAME):/work/profile \
                acidaniel/php7:sniffers phpcbf -s --colors \
                --standard=Drupal,DrupalPractice \
                --extensions=js \
                --ignore=*.css,*.md,*.info.yml,libraries/*,styleguide/* .
clean: info
	@echo "Removing networks for $(COMPOSE_PROJECT_NAME)"
ifeq ($(shell docker inspect --format="{{ .State.Running }}" $(COMPOSE_PROJECT_NAME)_web 2> /dev/null),true)
	docker-compose down
endif
