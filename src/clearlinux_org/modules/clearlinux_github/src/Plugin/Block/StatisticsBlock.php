<?php

namespace Drupal\clearlinux_github\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\clearlinux_github\GitHubDataService;

/**
 * Provides a 'StatisticsBlock' block.
 *
 * @Block(
 *  id = "statistics_block",
 *  admin_label = @Translation("Statistics block"),
 * )
 */
class StatisticsBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * Drupal\clearlinux_github\GitHubDataService definition.
   *
   * @var Drupal\clearlinux_github\GitHubDataService
   */
  protected $githubData;

  /**
   * Construct.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param string $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\clearlinux_github\GitHubDataService $githubData
   *   Array of github data defined.
   */
  public function __construct(
        array $configuration,
        $plugin_id,
        $plugin_definition,
        GitHubDataService $githubData
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->githubData = $githubData;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('github_data.api')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $build = [];
    $build['statistics_block'] = [
      '#theme' => 'statistics',
      '#statistics_data' => $this->githubData->getStatistics(),
    ];
    return $build;
  }

}
