<?php

namespace Drupal\clearlinux_github;

use Github\Client;
use Github\HttpClient\CachedHttpClient;
use Github\ResultPager;
use Drupal\Component\Serialization\Yaml;

/**
 * Class GitHubDataService.
 *
 * @package Drupal\clearlinux_github
 */
class GitHubDataService {
  /**
   * The client variable.
   *
   * @var \Github\Client
   *
   * The Client variable.
   */
  private $githubClient;

  /**
   * String.
   */
  const ORGANIZATION = 'clearlinux';

  /**
   * The client method with auth.
   */
  private function getGithubClient() {
    $config = '';

    if (!$this->githubClient) {
      $cacheDir = '/tmp/github-api-cache';
      $this->githubClient = new Client(
      new CachedHttpClient(['cache_dir' => $cacheDir])
      );
      if ($config_file = file_get_contents(DRUPAL_ROOT . '/../config.yml')) {
        $config = Yaml::decode($config_file);
        $this->githubClient->getHttpClient()->client->getConfig()->set('curl.options', ['CURLOPT_PROXY' => $config['variables']['proxy']['host'], 'CURLOPT_PROXYPORT' => $config['variables']['proxy']['port']]);
        $this->githubClient->authenticate($config['variables']['github_token'], NULL, Client::AUTH_HTTP_TOKEN);
      }
      else {
        return FALSE;
      }

    }

    return $this->githubClient;
  }

  /**
   * Used to get contributors of a given repository.
   *
   * @return array|mixed
   *
   *   Return count of contributors.
   */
  public function getAllContributors($repository) {
    $repoApi = $this->getGithubClient()->api('repo');
    $paginator = new ResultPager($this->githubClient);
    $parameters = [self::ORGANIZATION, $repository];
    $contributors = $paginator->fetchAll(
      $repoApi,
      'contributors',
      $parameters
    );

    return $contributors;
  }

  /**
   * Get Closed Pull Requests.
   */
  public function getClosedPullRequests($repository) {

    if ($config_file = file_get_contents(DRUPAL_ROOT . '/../config.yml')) {
      $config = Yaml::decode($config_file);
      $options = [
        'proxy' => $config['variables']['proxy']['host'] . ':' . $config['variables']['proxy']['port'],
      ];
    }

    $apiURL = sprintf(
      'https://api.github.com/repos/%s/%s/pulls?state=closed&per_page=1&access_token=' . $config['variables']['github_token'],
      self::ORGANIZATION,
      $repository
    );
    $client = new Client();
    $client->getHttpClient()->client->getConfig()->set('curl.options', ['CURLOPT_PROXY' => $config['variables']['proxy']['host'], 'CURLOPT_PROXYPORT' => $config['variables']['proxy']['port']]);
    $request = $client->getHttpClient()->get($apiURL, $options);
    $content = $request->getHeaders();
    if ($content['Link']) {
      $pagination = explode(',', $content['Link']);
      // Get only rel="last".
      $parts = str_replace('; rel="last"', '', $pagination[1]);
      $url_parts = parse_url($parts);
      parse_str($url_parts['query'], $query_parts);
      $closed_prs = str_replace('>', '', $query_parts['page']);

      return $closed_prs;
    }
  }

  /**
   * Main function to get all statics.
   *
   * @return array
   *
   *   Return an array of processed data from github.
   */
  public function getStatistics() {
    // For all repos in the organization.
    $repoApi = $this->getGithubClient()->api('organization');
    $paginator = new ResultPager($this->githubClient);
    $parameters = [self::ORGANIZATION];
    $repos = $paginator->fetchAll(
    $repoApi,
    'repositories',
    $parameters
    );
    foreach ($repos as $key => $value) {
      if (!$value['fork']) {
        $statistics['repo_name'][$key] = $value['name'];
        $statistics['contributors'][$key] = $this->getAllContributors($value['name']);
        $statistics['forks'][$key] = $value['forks'];
        $statistics['stars'][$key] = $value['stargazers_count'];
        $statistics['closed_pulls'][$key] = $this->getClosedPullRequests($value['name']);
      }
    }
    // Making array of github usernames (contributors).
    foreach (array_filter($statistics['contributors']) as $contributor) {
      foreach ($contributor as $contributor_value) {
        $unique_contributor_name[] = $contributor_value['login'];
      }
    }
    // Return data.
    return [
      'items' => [
          [
            'number' => array_sum($statistics['forks']),
            'description' => 'Forks',
          ],
          [
            'number' => number_format(count(array_unique($unique_contributor_name))),
            'description' => 'Contributors',
          ],
          [
            'number' => number_format(array_sum($statistics['stars'])),
            'description' => 'Stars',
          ],
          [
            'number' => array_sum($statistics['closed_pulls']),
            'description' => 'Closed Pull Requests',
          ],
      ],
    ];
  }

}
