<?php

namespace Drupal\clearlinux_rest\Plugin\rest\resource;

use Drupal\Core\Session\AccountProxyInterface;
use Drupal\rest\Plugin\ResourceBase;
use Drupal\rest\ModifiedResourceResponse;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\node\Entity\Node;
use Drupal\menu_link_content\Entity\MenuLinkContent;
use Psr\Log\LoggerInterface;

/**
 * Provides a resource for node entity.
 *
 * @RestResource(
 *   id = "clearlinux_documentation_post",
 *   label = @Translation("Clearlinux Post Documentation"),
 *   uri_paths = {
 *     "canonical" = "/api/postdocumentation",
 *     "https://www.drupal.org/link-relations/create" = "/api/postdocumentation",
 *   }
 * )
 */
class ApiDocumentationPost extends ResourceBase {

  /**
   * A current user instance.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   * Account Proxy Interface.
   */
  protected $currentUser;

  /**
   * Constructs a new ApiPostDocumentation object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param array $serializer_formats
   *   The available serialization formats.
   * @param \Psr\Log\LoggerInterface $logger
   *   A logger instance.
   * @param \Drupal\Core\Session\AccountProxyInterface $current_user
   *   A current user instance.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    array $serializer_formats,
    LoggerInterface $logger,
    AccountProxyInterface $current_user) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $serializer_formats, $logger);

    $this->currentUser = $current_user;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->getParameter('serializer.formats'),
      $container->get('logger.factory')->get('clearlinux_rest'),
      $container->get('current_user')
    );
  }

  /**
   * Returns stub response.
   *
   * @return \Drupal\rest\ResourceResponse
   *   The HTTP response object.
   */
  public function get() {
    // Unpublish all existing documentation nodes.
    $nids = \Drupal::entityQuery('node')->condition('type', 'documentation')->execute();
    $nodes = Node::loadMultiple($nids);
    $i = 0;
    foreach ($nodes as $node) {
      $node->setPublished(FALSE);
      $node->set('status', FALSE);
      $node->set('moderation_state', 'unpublished');
      $node->save();
      $i++;
    }
    return new ModifiedResourceResponse($i . ' Nodes were unpublished to be rebuild in the POST Method.');
  }

  /**
   * Responds to entity POST requests and saves the new entity.
   */
  public function post($entity = NULL) {
    $machineName = FALSE;
    $excludeFiles = [
      'disclaimers',
      'documentation_license',
      'genindex',
      'genindex-all',
      'search',
      'searchindex',
    ];
    // Ensure the data is posted.
    if ($entity) {
      if (isset($entity['current_page_name'])) {
        $machineName = $this->getMachineName($entity['current_page_name']);
        $firstLevel = $this->getFirstLevel($entity['current_page_name']);
        // Exclude unwanted files.
        if (in_array($machineName, $excludeFiles) || !isset($entity['body'])) {
          return new ModifiedResourceResponse("Invalid Data or Excluded in " . $machineName);
        }
      }
      if (isset($entity['body'])) {
        $body = strip_tags($entity['body'], '<dl><dt><dd><a><abbr><p><ol><code><img><pre><span><div><ul><li><h2><h3><h4><table><style><thead><tbody><th><tr><td><h1><strong><caption><em>');
      }
      $data = [
        'type' => 'documentation',
        'title' => $entity['title'],
        'display_toc' => $entity['display_toc'],
        'current_page_name' => $entity['current_page_name'],
        'body' => [
          'value' => $this->crossLinkReference($body, $machineName, $firstLevel),
        ],
        'status' => TRUE,
        'field_machine_name' => $machineName,
        'path' => [
          'alias' => $this->buildAlias($entity['current_page_name']),
        ],
        'menu_items' => $entity['parents'],
      ];
      return new ModifiedResourceResponse($this->createNode($data));
    }
    return new ModifiedResourceResponse("Error.");
  }

  /**
   * Get First Level.
   */
  protected function getFirstLevel($currentPage) {
    $firstLevel = explode('/', $currentPage);

    if (count($firstLevel) > 0) {
      return reset($firstLevel);
    }
    else {
      return $currentPage;
    }
  }

  /**
   * Build Alias.
   */
  protected function buildAlias($currentPage) {
    $base = '/documentation/';
    $alias = explode('/', $currentPage);
    $alias = array_unique($alias);
    if (count($alias > 1)) {
      return $base . implode('/', $alias);
    }
    else {
      return $base . $currentPage;
    }
  }

  /**
   * Current Page Name from Sphinx.
   */
  protected function nodeExist($data) {
    $sqlQuery = "SELECT DISTINCT(field_machine_name_value) AS machine_name, entity_id FROM node__field_machine_name;";
    $query = \Drupal::database()->query($sqlQuery);
    $result = $query->fetchAll();
    foreach ($result as $field_value) {
      $node = Node::load($field_value->entity_id);
      if ($field_value->machine_name == $data['field_machine_name']) {
        // If the node exist vs the data sent then we publish it.
        $node->setPublished(TRUE);
        $node->set('status', TRUE);
        $node->set('moderation_state', 'published');
        // Compare if changes were made.
        if (hash('md5', $node->body->value) != hash('md5', $data['body']['value']) || hash('md5', $node->title->value) != hash('md5', $data['title'])) {
          // If the body does not match then let's create a new revision.
          $node->setNewRevision(TRUE);
          $node->setRevisionLogMessage = t('Created new revision for this from sphinx');
          $node->title = $data['title'];
          $node->body->value = $data['body']['value'];
          $node->body->format = 'sphinx_text';
          $node->save();
          // Creating the Menu.
          $this->createMenuItems($node, $data);
          return ['The node ' . $node->id() . ' was updated successfully.'];
        }
        else {
          // Creating the Menu.
          $this->createMenuItems($node, $data);
          $node->save();
          return ['No new changes available for this content: ' . $data['field_machine_name']];
        }
      }
    }
  }

  /**
   * Function to fix links in body comming from sphinx to real nodes.
   */
  protected function crossLinkReference($body, $machineName, $firstLevel) {
    // Getting anchors and machine names.
    $anchor = '';
    $dom = new \DOMDocument('1.0', 'UTF-8');
    $dom->loadHTML(mb_convert_encoding($body, 'HTML-ENTITIES', 'UTF-8'), LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);
    $this->removeElementsByTagName('style', $dom);
    $xpath = new \DOMXPath($dom);
    $links = $xpath->query('//a[@class="reference internal"]/@href');
    // Update URL Cross Link and anchors.
    foreach ($links as $href) {
      // Remove ../ from the links.
      $clean_url = str_replace('../', '', $href->value);
      $clean_url = rtrim($clean_url, '/');
      // Check if Anchor Exists.
      if (FALSE !== $pos = strpos($clean_url, '#')) {
        $anchor = substr($clean_url, $pos);
        $clean_url = str_replace('/' . $anchor, '', $clean_url);
      }
      else {
        $anchor = '';
      }
      $linksSeparated = explode('/', $clean_url);
      $level = count($linksSeparated);
      $node_name = ($level == 1) ? reset($linksSeparated) : end($linksSeparated);
      $href->value = $this->getRealLink($node_name) . $anchor;
      $body = $dom->saveHTML($dom->documentElement) . PHP_EOL . PHP_EOL;
    }
    // Build images paths in case or return body.
    $body = $this->updateImagePaths($body);
    // Removing all h1 in the site.
    $dom->loadHTML(mb_convert_encoding($body, 'HTML-ENTITIES', 'UTF-8'), LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);
    $xpath = new \DOMXPath($dom);
    foreach ($xpath->query('//h1') as $node) {
      $node->parentNode->removeChild($node);
    }
    return $dom->saveHTML($dom->documentElement) . PHP_EOL . PHP_EOL;
  }

  /**
   * Protected function to remove tags and content.
   */
  public function removeElementsByTagName($tagName, $body) {
    $nodeList = $body->getElementsByTagName($tagName);
    for ($nodeIdx = $nodeList->length; --$nodeIdx >= 0;) {
      $node = $nodeList->item($nodeIdx);
      $node->parentNode->removeChild($node);
    }
  }

  /**
   * Function to fix images and link to internal images.
   */
  protected function updateImagePaths($body) {
    $img_path = '/sites/default/files/';
    $dom = new \DOMDocument('1.0', 'UTF-8');
    $dom->loadHTML(mb_convert_encoding($body, 'HTML-ENTITIES', 'UTF-8'), LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);
    $xpath = new \DOMXPath($dom);
    $imgs = $xpath->query('//img/@src');
    // Replacing images paths.
    foreach ($imgs as $img) {
      $clean_img = str_replace('../', '', $img->value);
      $img->value = $img_path . $clean_img;
      $body = $dom->saveHTML();
    }
    $imgs_links = $xpath->query('//a[@class="reference internal image-reference"]/@href');
    foreach ($imgs_links as $img_href) {
      $clean_img_path = str_replace('../', '', $img_href->value);
      $img_href->value = $img_path . $clean_img_path;
      $body = $dom->saveHTML($dom->documentElement) . PHP_EOL . PHP_EOL;
    }
    return $body;
  }

  /**
   * Search real link alias.
   */
  protected function getRealLink($node_name, $id = NULL) {
    $sqlQuery = "SELECT DISTINCT(f.field_machine_name_value) AS machine_name, f.entity_id, n.title AS title FROM node__field_machine_name f, node_field_data n WHERE n.nid = f.entity_id;";
    $query = \Drupal::database()->query($sqlQuery);
    $result = $query->fetchAll();
    foreach ($result as $field_value) {
      if ($field_value->machine_name == $node_name) {
        $nid = $field_value->entity_id;
        $title = $field_value->title;
        if ($id) {
          return ['id' => $nid, 'title' => $title];
        }
        else {
          $node = Node::load($nid);
          return $node->url();
        }
      }
    }
  }

  /**
   * Let's create the node.
   */
  protected function createNode($data) {
    $node = Node::create($data);
    $messages = [];
    $violations = $node->validate();
    if ($violations->count() > 0) {
      foreach ($violations as $violation) {
        $messages[] = $violation->getMessage()->render();
        if ($violation->getPropertyPath() == 'field_machine_name') {
          return $this->nodeExist($data);
        }
      }
      return ($messages);
    }
    else {
      $node->body->format = 'sphinx_text';
      $node->save();

      return ([
        "Node created with ID " => $node->id(),
        "Machine Name" => $data['field_machine_name'],
        "Title" => $data['title'],
      ]);
    }
  }

  /**
   * Function that will create Menu Items.
   */
  protected function createMenuItems($node, $data) {
    $parent_links = $data['menu_items'];
    $currentPage = $data['current_page_name'];

    if (!empty($currentPage) && !empty($parent_links)) {
      $links = explode('/', $currentPage);
      $links = array_unique($links);
      foreach ($links as $key => $value) {
        if ($key == 0) {
          // Getting real link for first level.
          $link = $this->getRealLink($value, TRUE);
          if ($link) {
            $sqlQuery = "SELECT id, title AS title FROM menu_link_content_data ";
            $sqlQuery .= "WHERE menu_name = 'documentation-menu' AND parent is NULL ";
            $sqlQuery .= "AND title = '" . $link['title'] . "';";
            $query = \Drupal::database()->query($sqlQuery);
            $result = $query->fetch();
            // If no result, let's create for first time menu item creation.
            if (!$result) {
              $first_level = MenuLinkContent::create([
                'title' => $link['title'],
                'link' => ['uri' => 'internal:/node/' . $link['id']],
                'menu_name' => 'documentation-menu',
                'expanded' => TRUE,
              ]);
              // Save first level.
              $first_level->save();
            }
            else {
              // If the menu exist let's just save the mid to.
              // relate the second level with the parent.
              $menuId1 = $result->id;
            }
          }
        }
        // Second Level.
        if ($key == 1) {
          // Getting real Link for second level item.
          $link = $this->getRealLink($value, TRUE);
          // If the parent item exist.
          if ($menuId1) {
            $sqlQuery = "SELECT id, uuid FROM menu_link_content WHERE id = " . $menuId1 . ";";
            $query = \Drupal::database()->query($sqlQuery);
            $result = $query->fetch();
            if ($result) {
              // Uuid from first level.
              $uuid = $result->uuid;
              // Checking if item exist in first level.
              $sqlSecond = "SELECT id, title FROM menu_link_content_data WHERE menu_name = 'documentation-menu' AND parent ='menu_link_content:" . $uuid . "';";
              $querySecond = \Drupal::database()->query($sqlSecond);
              $resultSecond = $querySecond->fetchAll();
              if ($resultSecond) {
                foreach ($resultSecond as $value_query) {
                  // Comparing in the dataset if title exist in the second level
                  // from the uuid of the first level.
                  if ($link['title'] == $value_query->title) {
                    // Since the title cannot be the same let's
                    // save the id of the second level item.
                    $menuId2 = $value_query->id;
                    break;
                  }
                }
              }
            }
            if (!$menuId2) {
              $second_level = MenuLinkContent::create([
                'title' => $link['title'],
                'link' => ['uri' => 'internal:/node/' . $link['id']],
                'menu_name' => 'documentation-menu',
                'parent' => 'menu_link_content:' . $uuid,
                'expanded' => TRUE,
              ]);
              $second_level->save();
            }
          }
        }
        // End of second level.
      }
    }
  }

  /**
   * Get the real machine name from current page..
   */
  protected function getMachineName($current_page_name) {

    $pageName = explode('/', $current_page_name);
    if (count($pageName) > 0) {
      return end($pageName);
    }
    else {
      return $current_page_name;
    }
  }

}
