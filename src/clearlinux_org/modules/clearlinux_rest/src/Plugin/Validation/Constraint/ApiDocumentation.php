<?php

namespace Drupal\clearlinux_rest\Plugin\Validation\Constraint;

use Symfony\Component\Validator\Constraint;

/**
 * Prevent duplicated values on machine_name field.
 *
 * @Constraint(
 *   id = "api_documentation",
 *   label = @Translation("Unique Machine Name", context = "Validation"),
 * )
 */
class ApiDocumentation extends Constraint {
  public $message = 'The machine name already exist';

}
