<?php

namespace Drupal\cl_test_rest_sphinx\Normalizer;

use Drupal\serialization\Normalizer\NormalizerBase;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;

/**
 * Normalize Interface Objects into a string.
 */
class ApiDataNormalizer extends NormalizerBase implements DenormalizerInterface {
  /**
   * The interface or class that this Normalizer supports.
   *
   * @var array
   */
  protected $supportedInterfaceOrClass = ['\Drupal\cl_test_rest_sphinx\NodeSerializeTrait'];

  /**
   * Denormalizes data back into an object of the given class.
   *
   * @param mixed $data
   *   Data to restore.
   * @param string $class
   *   The expected class to instantiate.
   * @param string $format
   *   Format the given data was extracted from.
   * @param array $context
   *   Options available to the denormalizer.
   *
   * @return Object
   *   Return the Object Data.
   */
  public function denormalize($data, $class, $format = NULL, array $context = []) {
    $test = new TestResource();

    $test->title = $data['title'];

    return $test;
  }

}
