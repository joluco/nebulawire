/**
 * @file
 */

(function ($) {
  Drupal.behaviors.ui = {
    attach: function (context) {

      $('.layout-node-form .layout-region-node-secondary:not(.open)').on('click',function () {
        $(this).addClass('open').prev().addClass('close');
      });

      $('.layout-node-form').on('click', '.layout-region-node-main.close',function () {
        $(this).removeClass('close').next().removeClass('open');
      });

    }
  }
})(jQuery);
