/**
 * @file
 */

(function ($) {
  "use strict";

  Drupal.behaviors.submenu = {
    attach: function (context, settings) {
      $("#header-choose-button").unbind().click(function () {
         $(this).toggleClass('active');
         $(".Header__container-submenu").toggleClass("Header__container-submenu-active");
     });
    }
  }

  Drupal.behaviors.code_snippet = {
    attach: function (context, settings) {

      $('body.nodetype--documentation .highlight').each(function(e) {
        $(this).attr('id', 'snippet_' + e);
        $(this).before('<button class="clipboard" data-clipboard-target="#snippet_'+ e +'"><i class="fa fa-copy"></i></button>');
      });
      new ClipboardJS('.clipboard', {
        target: function(trigger) {
          return trigger.nextElementSibling;
        }
      });
      // Trigger overlay on click copy/paste.
      $('body.nodetype--documentation').on('click', 'button.clipboard', (e) => {
        e.preventDefault();
        $('body.nodetype--documentation button.clipboard > .highlight').removeClass('tooltip');
        $(e.currentTarget).addClass('tooltip');
        $(e.currentTarget).mouseout(function() {
          $(e.currentTarget).removeClass('tooltip');
        });
      });
      // Mouse out to remove tooltip

  }
}

  Drupal.behaviors.search_form = {
    attach: function (context, settings) {
      $("body").on("click", ".Search .form-item", function (e) {
        var x = e.offsetX;
        var y = e.offsetY;
        var w = $(window).width();

        var hideMenu = function () {
          $("#block-projectmenu").hide();
          $("#block-clearlinux-theme-branding").hide();
        }

        var hideChoose = function () {
            $("#block-clearlinuxprojectnameblock").hide();
            $(".Header__choose-ico").hide();
        }

        var showMenu = function () {
          $("#block-projectmenu").show();
          $("#block-clearlinux-theme-branding").show();
          $("#block-clearlinuxprojectnameblock").show();
          $(".Header__choose-ico").show();
        }

        if (x >= -39 && x <= -10 && y >= 15 && y <= 35) {
          if (w <= 610) {
            hideMenu();
          }
          if (w >= 610 && w <= 1000) {
            hideChoose();
          }
          if (!$(this).hasClass('active')) {
            $(this).addClass('active');
          }
        }

        if (x >= 0 && x <= 31 && y >= 15 && y <= 35) {
          console.log('aca');
          if ($(this).hasClass('active')) {
            console.log("otro");
            $("#views-exposed-form-search-page").submit();
          }
        }

        if (x >= 255 && x <= 285 && y >= 15 && y <= 35 || (y >= 16 && y <= 38 && x >= 194 && x <= 218)) {
          if ($(this).hasClass('active')) {
            $(this).removeClass('active');
            $("#edit-keys").val("");
            showMenu();
          }
        }

      });
    }
  }
})(jQuery);
