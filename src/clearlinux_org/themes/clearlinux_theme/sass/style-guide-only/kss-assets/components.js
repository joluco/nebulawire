/**
 * @file
 */

(function ($) {

  $(document).ready(function () {

    $('.horizontal-tabs .tabs-mobile-header').click(function () {
      var $this = $(this);
      $this.parent().find('ul').toggle();
    });

    $('.horizontal-tabs .tabs a').on('click',function () {
      var $this = $(this),
          tab = $this.data('tab');

      $('.horizontal-tabs .tabs a').parent().removeClass('active')
      $('.horizontal-tabs .tab-content').removeClass('active');
      $this.parent().addClass('active');
      $('.horizontal-tabs  .tabs-mobile-header span').first().text($this.text());
      $($('.horizontal-tabs .tab-content')[tab]).addClass('active');

      if ($('.horizontal-tabs .tabs-mobile-header').is(':visible')) {
        $('.horizontal-tabs .tabs ul').hide();
      }

    });

    $('.cta-carousel-items').owlCarousel({
      loop:true,
      navText: [
        '<div class="cta-carousel-pager-prev"><i class="fa fa-angle-left"></i></div>',
        '<div class="cta-carousel-pager-next"><i class="fa fa-angle-right"></i></div>'
      ],
      responsive:{
        0:{
          items:2,
          margin:10,
          nav:false,
          stagePadding: 30,
        },
        510:{
          items:4,
          margin:40,
          nav: true,
          stagePadding: 0,
        },
        900:{
          items:6,
          margin:40,
          nav: true,
          stagePadding: 0,
        }
      }
    });

    $('.dynamic-block-container').owlCarousel({
      loop:true,
      dots:true,
      nav:true,
      navText: [
        '<i class="fa fa-angle-left"></i>',
        '<i class="fa fa-angle-right"></i>'
      ],
      responsive:{
        0:{
          items:1,
          margin:8,
          stagePadding: 60,
        },
        410:{
          items:1,
          margin:8,
          stagePadding: 110,
        },
        760:{
          items:2,
          margin:8,
          stagePadding: 110,
        },
        980:{
          items:3,
          margin:8,
          stagePadding: 110,
        },
        1200:{
          items:4,
          margin:8,
          stagePadding: 110,
        }
      },
      onInitialize: function () {
        setTimeout(function () {
          var margin = $('.dynamic-block-container .owl-dot').size() * 10;
          $('.dynamic-block-container .owl-prev').css('margin-right',(margin + 10) + 'px');
          $('.dynamic-block-container .owl-next').css('margin-left',(margin + 10) + 'px');
        },500);
      },
      onResize: function () {
        setTimeout(function () {
          var margin = $('.dynamic-block-container .owl-dot').size() * 10;
          $('.dynamic-block-container .owl-prev').css('margin-right',(margin + 10) + 'px');
          $('.dynamic-block-container .owl-next').css('margin-left',(margin + 10) + 'px');
        },500);
      }
    });

    var owl1 = $('.hero-carousel-slider-container').owlCarousel({
      dots:true,
      nav:false,
      items:1,
    });

    var owl2 = $('.hero-carousel-mobile').owlCarousel({
      nav:false,
      items:1,
    });

    var owl1index = 0;
    var owl2index = 0;

    owl1.on('changed.owl.carousel', function (event) {
      if (event.item.index != owl2index) {
        owl2.trigger('to.owl.carousel', [event.item.index,300,true]);
        owl2index = event.item.index
      }
    });

    owl2.on('changed.owl.carousel', function (event) {
      if (event.item.index != owl1index) {
        owl1.trigger('to.owl.carousel', [event.item.index,300,true]);
        owl1index = event.item.index
      }
    });

  });

})(jQuery);
