/**
 * @file
 */

(function ($) {
  $('.tabs__table_of_content').bind('click', tableOfContentAction);
  $('.tabs__menu_list_parent__icon').bind('click', tabItemAction);
  $('.tabs nav a').bind('click', _closeDrawer);
  $('#tabs__overlay').bind('click', _closeDrawer);

  /* Close opened drawers if we passed from mobile to desktop res. */
  $(window).resize(function(){
    if ($(window).width() <= 1024) {
      _closeDrawer();
    }
  });

  function tabItemAction(event) {
    if ($(window).width() <= 1024) {
      $(this).parent().toggleClass('collapsed');
      $(this).parent().find('ul').first().toggleClass('collapsed');
      $(this).parent().find('i').first().toggleClass('fa-caret-down fa-caret-right');
    }
  }

  function tableOfContentAction(event){
    $(this).next().toggleClass('opened');
    $('#tabs__overlay').toggleClass('active');
  }

  function _closeDrawer(){
    $('#tabs__overlay').removeClass('active');
    $('.tabs__menu.opened').each(function(){
      $(this).removeClass('opened');
    });
  }
})(jQuery);
