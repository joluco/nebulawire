/**
 * @file
 */

(function($) {
  $('.sidebar_nav__menu__list__parent_icon').bind('click', sidebarItemAction);
  function sidebarItemAction(event) {
    $(this).toggleClass('fa-caret-down fa-caret-right');
    $(this).parent().parent().parent().toggleClass('collapsed');
    $(this).parent().parent().parent().find('ul').first().toggleClass('collapsed');
  }

})(jQuery);
