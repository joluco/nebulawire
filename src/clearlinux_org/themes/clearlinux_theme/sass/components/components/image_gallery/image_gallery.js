/**
 * @file
 */

(function ($) {
  $(document).ready(function () {
    /* Init all carousels with the most adecuate config. */
    $.each($(".image_gallery"), function(){
      /* Setting dafault config for a single carousel. */
      var items = $(this).children(), config = {
        autoplay: true,
        autoplayHoverPause: true,
        loop: true,
        nav: false,
        autoWidth: false,
        responsive:{
          0: {
            items: 1,
            touchDrag: true,
            mouseDrag: true,
            margin: 0,
            nav: false,
            dots: true
          },
          1025: {
            items: 4,
            touchDrag: true,
            mouseDrag: true,
            margin: 5,
            dots: false
          }
        }
      }
      /* If the carousel have less than 3 items then we change the config */
      if ( (items.length <= 3 && $(window).width() > 1024)
        || (items.length <= 1 && $(window).width() <= 1024) ) {
        config.autoplay = false;
        config.nav = false;
        config.autoWidth = true;
      }
      /* Corousel init only if we have more than 1 item. */
      if (items.length > 1) {
        $(this).owlCarousel(config);
      }
    });

  });

})(jQuery);
