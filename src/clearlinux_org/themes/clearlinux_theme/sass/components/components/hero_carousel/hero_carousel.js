/**
 * @file
 */

(function ($) {
  $(document).ready(function () {
    $(".Carousel").owlCarousel({
      autoplay: false,
      autoplayTimeout:6000,
      autoplayHoverPause:true,
      dots: false,
      responsiveClass:true,
      responsive:{
          0:{
              items:1,
              nav:true,
              loop:true
          },
          600:{
              items:1,
              nav:true,
              loop:true
          },
          1000:{
              items:1,
              nav:true,
              loop:true
          }
      }
    });
  });
})(jQuery);
