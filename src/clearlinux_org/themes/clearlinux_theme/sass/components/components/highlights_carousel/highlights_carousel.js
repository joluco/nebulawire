/**
 * @file
 */

(function ($) {
  $(document).ready(function () {
    /* Init all carousels with the most adecuate config. */
    $.each($(".highlights_carousel"), function(){
      /* Setting dafault config for a single carousel. */
      var items = $(this).children(), config = {
        autoplay: false,
        loop: true,
        nav: true,
        autoWidth: false,
        scrollPerPage: true,
        slideBy: 'page',
        responsive:{
          0: {
            items: 1,
            touchDrag: true,
            mouseDrag: true,
            nav: false
          },
          1025: {
            items: 3,
            touchDrag: false,
            mouseDrag: false
          }
        }
      }
      /* Corousel init only if we have more than 1 item. */
      if (items.length > 1) {
        $(this).owlCarousel(config);
      }
    });

  });

})(jQuery);
