/**
 * @file
 */

(function ($) {
  $("body").on("click", ".Search .form-item", function (e) {
    var x = e.offsetX;
    var y = e.offsetY;
    var w = $(window).width();
    console.log(w);

    var hideMenu = function () {
      $("#block-projectmenu").hide();
      $("#block-clearlinux-theme-branding").hide();
      $("#block-clearlinuxprojectnameblock").hide();
    }

    var showMenu = function () {
      $("#block-projectmenu").show();
      $("#block-clearlinux-theme-branding").show();
      $("#block-clearlinuxprojectnameblock").show();
    }

    if (x >= -39 && x <= -10 && y >= 15 && y <= 35) {
      if (w <= 610) {
        hideMenu();
      }
      if (!$(this).hasClass('active')) {
        $(this).addClass('active');
      }
    }

    if (x >= 0 && x <= 31 && y >= 15 && y <= 35) {
      if ($(this).hasClass('active')) {
        $("#search-block-form").submit();
      }
    }

    if (x >= 291 && x <= 318 && y >= 15 && y <= 35) {
      if ($(this).hasClass('active')) {
        $(this).removeClass('active');
        $("#edit-keys").val("");
        showMenu();
      }
    }

  });
})(jQuery);
