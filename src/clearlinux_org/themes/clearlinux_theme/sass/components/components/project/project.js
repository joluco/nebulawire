/**
 * @file
 */

(function ($) {
  $(document).ready(function () {
    $(".Project").owlCarousel({
      autoplay: false,
      dots: false,
      responsiveClass: true,
      responsive: {
        0: {
          items: 1,
          nav: true,
          loop: true
        },
        768: {
          items: 2,
          nav: true,
          loop: true
        },
        1200: {
          items: 3,
          nav: false,
          loop: false
        }
      }
    });
  });
})(jQuery);
