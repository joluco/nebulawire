/**
 * @file
 */

(function ($) {
  $(document).ready(function () {

    $(".Community_container").owlCarousel({
      autoplay: false,
      autoplayTimeout:6000,
      autoplayHoverPause:true,
      dots: true,
      responsiveClass:true,
      responsive:{
          0:{
              items:1,
              nav:true,
              loop:true
          },
          600:{
              items:2,
              nav:true,
              loop:true
          },
          1000:{
              items:4,
              nav:true,
              loop:false
          }
      }
    });
  });
})(jQuery);
