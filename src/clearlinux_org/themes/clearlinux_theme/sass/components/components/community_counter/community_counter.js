/**
 * @file
 */

(function ($) {
  $(document).ready(function () {
    $(".community_counter__list").owlCarousel({
      autoplay: false,
      autoplayTimeout: 6000,
      autoplayHoverPause: true,
      center: true,
      loop: true,
      dots: false,
      nav: true,
      responsive:{
        0: {
          items: 1,
        },
        768: {
          items: 3,
        }
      }
    });

    var a = 0;
    $(window).scroll(function() {
      $('.community_counter__list').each(function(){
        var oTop = $(this).offset().top - window.innerHeight;
        if (a == 0 && $(window).scrollTop() > oTop) {
          $(this).find('.community_counter__list__item__number').each(function() {
            var $this = $(this),
              countTo = $this.attr('data-count');
              countTo = parseInt(countTo.replace(/,/g, ''));
            $({
              countNum: 0
            }).animate({
                countNum: countTo
              },
              {
                duration: 800,
                easing: 'swing',
                step: function() {
                  $this.text(commaSeparateNumber(Math.floor(this.countNum)));
                },
                complete: function() {
                  $this.text(commaSeparateNumber(this.countNum));
                }
              });
          });
          a = 1;
        }
        });
    });

    function commaSeparateNumber(val){
      while (/(\d+)(\d{3})/.test(val.toString())){
        val = val.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
      }
      return val;
    }
  });
})(jQuery);
