<?php

namespace Drupal\clearlinux_org\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Routing\CurrentRouteMatch;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a 'ToolbarBlock' block.
 *
 * @Block(
 *  id = "toolbar_block",
 *  admin_label = @Translation("Toolbar block"),
 * )
 */
class ToolbarBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * Drupal\Core\Routing\CurrentRouteMatch definition.
   *
   * @var \Drupal\Core\Routing\CurrentRouteMatch
   */
  protected $currentRouteMatch;
  /**
   * Drupal\Core\Config\ConfigFactory definition.
   *
   * @var \Drupal\Core\Config\ConfigFactory
   */
  protected $configFactory;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a new DefaultBlock object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param string $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Routing\CurrentRouteMatch $current_route_match
   *   The current_route_match url request.
   * @param \Drupal\Core\Config\ConfigFactory $config_factory
   *   The config_factory array with params.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity Type Manager.
   */
  public function __construct(
     array $configuration,
     $plugin_id,
     $plugin_definition,
     CurrentRouteMatch $current_route_match,
     ConfigFactory $config_factory,
     EntityTypeManagerInterface $entity_type_manager
    ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->currentRouteMatch = $current_route_match;
    $this->configFactory = $config_factory;
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
    $configuration,
    $plugin_id,
    $plugin_definition,
    $container->get('current_route_match'),
    $container->get('config.factory'),
    $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $categories = [];
    if ($node = $this->currentRouteMatch->getParameter('node')) {
      $toolbar['node_created'] = date('d M, Y', $node->getCreatedTime());
      if (!empty($node->getOwner()->get('field_first_name')->getValue()) && !empty($node->getOwner()->get('field_last_name'))) {
        $toolbar['node_author'] = $node->getOwner()->get('field_first_name')->getValue()[0]['value'] . ' ' . $node->getOwner()->get('field_last_name')->getValue()[0]['value'];
      }
      else {
        $toolbar['node_author'] = $node->getOwner()->getDisplayName();
      }
      if ($node->hasField('field_category')) {
        for ($i = 0; $i < count($node->field_category->getValue()); $i++) {
          $term_obj = $this->entityTypeManager->getStorage('taxonomy_term')->load($node->field_category->getValue()[$i]['target_id']);
          $categories[$i]['title'] = $term_obj->getName();
          $categories[$i]['href'] = '/blogs?category=' . $node->field_category->getValue()[$i]['target_id'];
        }
        $toolbar['node_categories'] = $categories;
      }
      return $toolbar;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheTags() {
    // With this when your node change your block will rebuild.
    if ($node = $this->currentRouteMatch->getParameter('node')) {
      // If there is node add its cachetag.
      return Cache::mergeTags(parent::getCacheTags(), ['node:' . $node->id()]);
    }
    else {
      // Return default tags instead.
      return parent::getCacheTags();
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheContexts() {
    // If you depends on \Drupal::routeMatch()
    // you must set context of this block with 'route' context tag.
    // Every new route this block will rebuild.
    return Cache::mergeContexts(parent::getCacheContexts(), ['route']);
  }

  /**
   * Custom function to build styled project title.
   */
  public function getProjectTitle($content) {
    $content = explode(' ', $content);
    $projectName = '';
    foreach ($content as $key => $value) {
      if ($key == 0) {
        $strongName = '<strong>' . $value . '</strong>';
      }
      else {
        $projectName .= ' ' . $value;
      }
    }
    $content = $strongName . $projectName;
    return $content;
  }

}
