<?php

use Drupal\file\Entity\File;

namespace Drupal\clearlinux_org\Twig\Extension;

/**
 * Provides embed video filters for Twig templates.
 */
class CustomFiltersExtension extends \Twig_Extension {

  /**
   * {@inheritdoc}
   */
  public function getFilters() {
    return [
      new \Twig_SimpleFilter('T', [$this, 'getFieldEmbedUrl']),
      new \Twig_SimpleFilter('get_video_id', [$this, 'getVideoId']),
      new \Twig_SimpleFilter('thumbnail_video', [$this, 'getThumbnail']),
      new \Twig_SimpleFilter('file_url_id', [$this, 'getFileUrl']),
      new \Twig_SimpleFilter('file_info', [$this, 'getFileInfo']),
    ];
  }

  /**
   * Gets a unique identifier for this Twig extension.
   */
  public function getName() {
    return 'twig_clearlinux_custom_filters.twig_extension';
  }

  /**
   * Twig filter callback: return and id of video.
   *
   * @param string $url
   *   Regular url.
   *
   * @return video_id
   *   Embeddable url for youtube or vimeo.
   */
  public static function getVideoId($url) {
    $id = NULL;

    if (preg_match('/https:\/\/(?:www.)?(youtube).com\/watch\\?v=(.*?)/', $url)) {
      $id = preg_replace("/\s*[a-zA-Z\/\/:\.]*youtube.com\/watch\?v=([a-zA-Z0-9\-_]+)([a-zA-Z0-9\/\*\-\_\?\&\;\%\=\.]*)/i", "$1", $url);
    }
    if (preg_match('/https:\/\/youtu.be\/([a-zA-Z0-9\-_])/', $url)) {
      $id = preg_replace("/\s*[a-zA-Z\/\/:\.]*youtu.be\/([a-zA-Z0-9\-_]+)([a-zA-Z0-9\/\*\-\_\?\&\;\%\=\.]*)/i", "$1", $url);
    }
    if (preg_match('/https:\/\/vimeo.com\/(\\d+)/', $url, $regs)) {
      $id = $regs[1];
    }
    if (preg_match('/https:\/\/www.youtube.com\/embed\/(.*)/', $url, $regs)) {
      $id = $regs[1];
    }
    return $id;
  }

  /**
   * Twig filter callback: return and embeddable url.
   *
   * @param string $url
   *   Regular url.
   *
   * @return embed_url
   *   Embeddable url for youtube or vimeo.
   */
  public static function getFieldEmbedUrl($url) {
    $embed = NULL;

    if (preg_match('/https:\/\/(?:www.)?(youtube).com\/watch\\?v=(.*?)/', $url)) {
      $embed = preg_replace("/\s*[a-zA-Z\/\/:\.]*youtube.com\/watch\?v=([a-zA-Z0-9\-_]+)([a-zA-Z0-9\/\*\-\_\?\&\;\%\=\.]*)/i", "https://www.youtube.com/embed/$1", $url);
    }
    if (preg_match('/https:\/\/youtu.be\/([a-zA-Z0-9\-_])/', $url)) {
      $embed = preg_replace("/\s*[a-zA-Z\/\/:\.]*youtu.be\/([a-zA-Z0-9\-_]+)([a-zA-Z0-9\/\*\-\_\?\&\;\%\=\.]*)/i", "https://youtube.com/embed/$1", $url);
    }
    if (preg_match('/https:\/\/vimeo.com\/(\\d+)/', $url, $regs)) {
      $embed = 'https://player.vimeo.com/video/' . $regs[1];
    }
    return $embed;
  }

  /**
   * Twig filter callback: return and embeddable url.
   *
   * @param string $url
   *   Regular url.
   *
   * @return thumbnail_image
   *   thumbnail image for youtube or vimeo.
   */
  public static function getThumbnail($url) {
    $image = NULL;
    if (preg_match('/https:\/\/(?:www.)?(youtube).com\/watch\\?v=(.*?)/', $url)) {
      $big_image = preg_replace("/\s*[a-zA-Z\/\/:\.]*youtube.com\/watch\?v=([a-zA-Z0-9\-_]+)([a-zA-Z0-9\/\*\-\_\?\&\;\%\=\.]*)/i", "https://img.youtube.com/vi/$1/maxresdefault.jpg", $url);
      $small_image = preg_replace("/\s*[a-zA-Z\/\/:\.]*youtube.com\/watch\?v=([a-zA-Z0-9\-_]+)([a-zA-Z0-9\/\*\-\_\?\&\;\%\=\.]*)/i", "https://img.youtube.com/vi/$1/sddefault.jpg", $url);

      $file_headers = @get_headers($big_image);
      if (!$file_headers || $file_headers[0] == 'HTTP/1.0 404 Not Found') {
        $image = $small_image;
      }
      else {
        $image = $big_image;
      }
    }
    if (preg_match('/https:\/\/youtu.be\/([a-zA-Z0-9\-_])/', $url)) {
      $big_image = preg_replace("/\s*[a-zA-Z\/\/:\.]*youtu.be\/([a-zA-Z0-9\-_]+)([a-zA-Z0-9\/\*\-\_\?\&\;\%\=\.]*)/i", "https://img.youtube.com/vi/$1/maxresdefault.jpg", $url);
      $small_image = preg_replace("/\s*[a-zA-Z\/\/:\.]*youtu.be\/([a-zA-Z0-9\-_]+)([a-zA-Z0-9\/\*\-\_\?\&\;\%\=\.]*)/i", "https://img.youtube.com/vi/$1/sddefault.jpg", $url);

      $file_headers = @get_headers($big_image);
      if (!$file_headers || $file_headers[0] == 'HTTP/1.0 404 Not Found') {
        $image = $small_image;
      }
      else {
        $image = $big_image;
      }
    }
    if (preg_match('/https:\/\/vimeo.com\/(\\d+)/', $url, $regs)) {
      $vimeo = unserialize(file_get_contents("http://vimeo.com/api/v2/video/" . $regs[1] . ".php"));
      $image = $vimeo[0]['thumbnail_large'];
    }
    return $image;
  }

  /**
   * Twig filter callback: return file url.
   *
   * @param int $fid
   *   Id of the file.
   *
   * @return file_url
   *   Public url of file.
   */
  public static function getFileUrl($fid) {
    $path = "";
    $file = File::load($fid);
    $path = $file->url();
    return $path;
  }

  /**
   * Twig filter callback: return file extension.
   *
   * @param string $file
   *   Url of the file.
   * @param string $info
   *   Type of file, metadata.
   *
   * @return file_extension
   *   File information.
   */
  public static function getFileInfo($file, $info = NULL) {
    $file_data = pathinfo($file);
    $output = NULL;

    if (is_array($file_data)) {
      if (isset($file_data['extension'])) {
        $output = $file_data['extension'];
      }

      if ($info == 'filename') {
        $output = $file_data['filename'];
      }
    }

    return $output;
  }

}
