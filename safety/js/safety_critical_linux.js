$(document).ready(function(){
  $('a[href^="#"]').click(function (evt) {
      evt.preventDefault();

      var target = this.hash,
          $target = $(target);

      $('html, body').stop().animate({
        'scrollTop': $target.offset().top
      }, 500, function () {
        window.location.hash = target;
      });

      return false;
  });

  $('.menu-anchors').affix({
    offset: {
      top: $('header').outerHeight(true) + $('.gradient--hero').outerHeight(true),
      bottom: function () {
        return (this.bottom = $('footer').outerHeight(true))
      }
    }
  });

  $('.faq-list dt').click(function(){
    $(this).toggleClass('faq--expanded');
    $(this).next().stop(true, true).slideToggle();
  });
});
